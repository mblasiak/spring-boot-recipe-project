package pl.blazzmi.recipeproject.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.blazzmi.recipeproject.domain.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {
}
