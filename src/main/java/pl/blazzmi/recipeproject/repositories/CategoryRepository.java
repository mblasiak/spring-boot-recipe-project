package pl.blazzmi.recipeproject.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.blazzmi.recipeproject.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
