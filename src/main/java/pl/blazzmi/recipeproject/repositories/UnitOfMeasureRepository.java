package pl.blazzmi.recipeproject.repositories;

import org.springframework.data.repository.CrudRepository;
import pl.blazzmi.recipeproject.domain.UnitOfMeasure;

public interface UnitOfMeasureRepository extends CrudRepository<UnitOfMeasure, Long> {

}
